package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application implements EventHandler<ActionEvent> {

    Button button;
    String desiredDataFromInput;
    TextField textField;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Extract value to string");
        button = new Button();
        button.setText("Click to extract");
        textField = new TextField();
        button.setOnAction(this);
        VBox layout = new VBox();
        layout.getChildren().addAll(textField, button);
        Scene scene = new Scene(layout, 300, 300);
        primaryStage.setScene(scene);
        primaryStage.show();
        System.out.println(desiredDataFromInput);
    }

    @Override
    public void handle(ActionEvent event) {
        if (event.getSource() == button) {
            desiredDataFromInput = textField.getText();
            System.out.println(desiredDataFromInput);
        }
    }
}
